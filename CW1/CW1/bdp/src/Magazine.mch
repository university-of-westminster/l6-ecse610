﻿/* Magazine
 * Author: Martin Kukura
 * Creation date: 09/12/2016
 */
MACHINE
    Magazine

SEES
    Shared

SETS
    MAGAZINES  = { NewScientist , PrivateEye , Beano , RadioTimes , Vogue , Hello }

ABSTRACT_VARIABLES
    house_magazine

INVARIANT
    house_magazine : available_houses <-> MAGAZINES

INITIALISATION
    house_magazine := {}


OPERATIONS
    ret <-- magazine_requestMagazineDelivery ( house , magazine ) =
    PRE
        house : available_houses &
        magazine : MAGAZINES
    THEN
        IF ( ( house |-> magazine ) : house_magazine )
        THEN ret := HouseAlreadyDelivering
        ELSIF ( card ( { house } <| house_magazine ) >= 3 )
        THEN ret := MagazineLimitReached
        ELSE
            house_magazine := house_magazine \/ { house |-> magazine } ||
            ret := Success
        END
    END ;

    ret <-- cancelMagazineDelivery ( house , magazine ) =
    PRE
        house : available_houses &
        magazine : MAGAZINES
    THEN
        IF ( ( house |-> magazine ) : house_magazine )
        THEN
            house_magazine := house_magazine - { house |-> magazine } ||
            ret := Success
        ELSE
            ret := HouseNotDelivering
        END
    END ;

    ret <-- whatMagazinesDelivered ( house ) =
    PRE
        house : available_houses
    THEN
        ret := ran ( { house } <| house_magazine )
    END ;

    ret <-- whatMagazinesSold =
    BEGIN
        ret := ran ( house_magazine )
    END ;

    ret <-- magazinesSold ( magazine ) =
    PRE
        magazine : MAGAZINES
    THEN
        ret := card ( house_magazine |> { magazine } )
    END

END
