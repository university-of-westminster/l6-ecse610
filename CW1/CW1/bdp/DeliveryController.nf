﻿Normalised(
THEORY MagicNumberX IS
  MagicNumber(Machine(DeliveryController))==(3.5)
END
&
THEORY UpperLevelX IS
  First_Level(Machine(DeliveryController))==(Machine(DeliveryController));
  Level(Machine(DeliveryController))==(0)
END
&
THEORY LoadedStructureX IS
  Machine(DeliveryController)
END
&
THEORY ListSeesX IS
  List_Sees(Machine(DeliveryController))==(Shared)
END
&
THEORY ListUsesX IS
  List_Uses(Machine(DeliveryController))==(?)
END
&
THEORY ListIncludesX IS
  Inherited_List_Includes(Machine(DeliveryController))==(Magazine,Newspaper);
  List_Includes(Machine(DeliveryController))==(Newspaper,Magazine)
END
&
THEORY ListPromotesX IS
  List_Promotes(Machine(DeliveryController))==(requestPaperDelivery,housesDeliveredTo,whatPaperDelivered,whatPapersSold,papersSold,cancelMagazineDelivery,whatMagazinesDelivered,whatMagazinesSold,magazinesSold)
END
&
THEORY ListExtendsX IS
  List_Extends(Machine(DeliveryController))==(?)
END
&
THEORY ListVariablesX IS
  External_Context_List_Variables(Machine(DeliveryController))==(?);
  Context_List_Variables(Machine(DeliveryController))==(?);
  Abstract_List_Variables(Machine(DeliveryController))==(?);
  Local_List_Variables(Machine(DeliveryController))==(?);
  List_Variables(Machine(DeliveryController))==(house_newspaper,house_magazine);
  External_List_Variables(Machine(DeliveryController))==(house_newspaper,house_magazine)
END
&
THEORY ListVisibleVariablesX IS
  Inherited_List_VisibleVariables(Machine(DeliveryController))==(?);
  Abstract_List_VisibleVariables(Machine(DeliveryController))==(?);
  External_List_VisibleVariables(Machine(DeliveryController))==(?);
  Expanded_List_VisibleVariables(Machine(DeliveryController))==(?);
  List_VisibleVariables(Machine(DeliveryController))==(?);
  Internal_List_VisibleVariables(Machine(DeliveryController))==(?)
END
&
THEORY ListInvariantX IS
  Gluing_Seen_List_Invariant(Machine(DeliveryController))==(btrue);
  Gluing_List_Invariant(Machine(DeliveryController))==(btrue);
  Abstract_List_Invariant(Machine(DeliveryController))==(btrue);
  Expanded_List_Invariant(Machine(DeliveryController))==(house_newspaper: available_houses +-> NEWSPAPERS & house_magazine: available_houses <-> MAGAZINES);
  Context_List_Invariant(Machine(DeliveryController))==(btrue);
  List_Invariant(Machine(DeliveryController))==(btrue)
END
&
THEORY ListAssertionsX IS
  Abstract_List_Assertions(Machine(DeliveryController))==(btrue);
  Expanded_List_Assertions(Machine(DeliveryController))==(btrue);
  Context_List_Assertions(Machine(DeliveryController))==(btrue);
  List_Assertions(Machine(DeliveryController))==(btrue)
END
&
THEORY ListCoverageX IS
  List_Coverage(Machine(DeliveryController))==(btrue)
END
&
THEORY ListExclusivityX IS
  List_Exclusivity(Machine(DeliveryController))==(btrue)
END
&
THEORY ListInitialisationX IS
  Expanded_List_Initialisation(Machine(DeliveryController))==(house_newspaper:={};house_magazine:={});
  Context_List_Initialisation(Machine(DeliveryController))==(skip);
  List_Initialisation(Machine(DeliveryController))==(skip)
END
&
THEORY ListParametersX IS
  List_Parameters(Machine(DeliveryController))==(?)
END
&
THEORY ListInstanciatedParametersX IS
  List_Instanciated_Parameters(Machine(DeliveryController),Machine(Newspaper))==(?);
  List_Instanciated_Parameters(Machine(DeliveryController),Machine(Magazine))==(?);
  List_Instanciated_Parameters(Machine(DeliveryController),Machine(Shared))==(?)
END
&
THEORY ListConstraintsX IS
  List_Constraints(Machine(DeliveryController),Machine(Magazine))==(btrue);
  List_Context_Constraints(Machine(DeliveryController))==(btrue);
  List_Constraints(Machine(DeliveryController))==(btrue);
  List_Constraints(Machine(DeliveryController),Machine(Newspaper))==(btrue)
END
&
THEORY ListOperationsX IS
  Internal_List_Operations(Machine(DeliveryController))==(cancelPaperDelivery,requestMagazineDelivery,requestPaperDelivery,housesDeliveredTo,whatPaperDelivered,whatPapersSold,papersSold,cancelMagazineDelivery,whatMagazinesDelivered,whatMagazinesSold,magazinesSold);
  List_Operations(Machine(DeliveryController))==(cancelPaperDelivery,requestMagazineDelivery,requestPaperDelivery,housesDeliveredTo,whatPaperDelivered,whatPapersSold,papersSold,cancelMagazineDelivery,whatMagazinesDelivered,whatMagazinesSold,magazinesSold)
END
&
THEORY ListInputX IS
  List_Input(Machine(DeliveryController),magazinesSold)==(magazine);
  List_Input(Machine(DeliveryController),whatMagazinesSold)==(?);
  List_Input(Machine(DeliveryController),whatMagazinesDelivered)==(house);
  List_Input(Machine(DeliveryController),cancelMagazineDelivery)==(house,magazine);
  List_Input(Machine(DeliveryController),papersSold)==(paper);
  List_Input(Machine(DeliveryController),whatPapersSold)==(?);
  List_Input(Machine(DeliveryController),whatPaperDelivered)==(house);
  List_Input(Machine(DeliveryController),housesDeliveredTo)==(?);
  List_Input(Machine(DeliveryController),requestPaperDelivery)==(house,paper);
  List_Input(Machine(DeliveryController),cancelPaperDelivery)==(house);
  List_Input(Machine(DeliveryController),requestMagazineDelivery)==(house,magazine)
END
&
THEORY ListOutputX IS
  List_Output(Machine(DeliveryController),magazinesSold)==(ret);
  List_Output(Machine(DeliveryController),whatMagazinesSold)==(ret);
  List_Output(Machine(DeliveryController),whatMagazinesDelivered)==(ret);
  List_Output(Machine(DeliveryController),cancelMagazineDelivery)==(ret);
  List_Output(Machine(DeliveryController),papersSold)==(ret);
  List_Output(Machine(DeliveryController),whatPapersSold)==(ret);
  List_Output(Machine(DeliveryController),whatPaperDelivered)==(ret);
  List_Output(Machine(DeliveryController),housesDeliveredTo)==(ret);
  List_Output(Machine(DeliveryController),requestPaperDelivery)==(ret);
  List_Output(Machine(DeliveryController),cancelPaperDelivery)==(ret);
  List_Output(Machine(DeliveryController),requestMagazineDelivery)==(ret)
END
&
THEORY ListHeaderX IS
  List_Header(Machine(DeliveryController),magazinesSold)==(ret <-- magazinesSold(magazine));
  List_Header(Machine(DeliveryController),whatMagazinesSold)==(ret <-- whatMagazinesSold);
  List_Header(Machine(DeliveryController),whatMagazinesDelivered)==(ret <-- whatMagazinesDelivered(house));
  List_Header(Machine(DeliveryController),cancelMagazineDelivery)==(ret <-- cancelMagazineDelivery(house,magazine));
  List_Header(Machine(DeliveryController),papersSold)==(ret <-- papersSold(paper));
  List_Header(Machine(DeliveryController),whatPapersSold)==(ret <-- whatPapersSold);
  List_Header(Machine(DeliveryController),whatPaperDelivered)==(ret <-- whatPaperDelivered(house));
  List_Header(Machine(DeliveryController),housesDeliveredTo)==(ret <-- housesDeliveredTo);
  List_Header(Machine(DeliveryController),requestPaperDelivery)==(ret <-- requestPaperDelivery(house,paper));
  List_Header(Machine(DeliveryController),cancelPaperDelivery)==(ret <-- cancelPaperDelivery(house));
  List_Header(Machine(DeliveryController),requestMagazineDelivery)==(ret <-- requestMagazineDelivery(house,magazine))
END
&
THEORY ListOperationGuardX END
&
THEORY ListPreconditionX IS
  Own_Precondition(Machine(DeliveryController),magazinesSold)==(magazine: MAGAZINES);
  List_Precondition(Machine(DeliveryController),magazinesSold)==(magazine: MAGAZINES);
  Own_Precondition(Machine(DeliveryController),whatMagazinesSold)==(btrue);
  List_Precondition(Machine(DeliveryController),whatMagazinesSold)==(btrue);
  Own_Precondition(Machine(DeliveryController),whatMagazinesDelivered)==(house: available_houses);
  List_Precondition(Machine(DeliveryController),whatMagazinesDelivered)==(house: available_houses);
  Own_Precondition(Machine(DeliveryController),cancelMagazineDelivery)==(house: available_houses & magazine: MAGAZINES);
  List_Precondition(Machine(DeliveryController),cancelMagazineDelivery)==(house: available_houses & magazine: MAGAZINES);
  Own_Precondition(Machine(DeliveryController),papersSold)==(paper: NEWSPAPERS);
  List_Precondition(Machine(DeliveryController),papersSold)==(paper: NEWSPAPERS);
  Own_Precondition(Machine(DeliveryController),whatPapersSold)==(btrue);
  List_Precondition(Machine(DeliveryController),whatPapersSold)==(btrue);
  Own_Precondition(Machine(DeliveryController),whatPaperDelivered)==(house: available_houses & house: dom(house_newspaper));
  List_Precondition(Machine(DeliveryController),whatPaperDelivered)==(house: available_houses & house: dom(house_newspaper));
  Own_Precondition(Machine(DeliveryController),housesDeliveredTo)==(btrue);
  List_Precondition(Machine(DeliveryController),housesDeliveredTo)==(btrue);
  Own_Precondition(Machine(DeliveryController),requestPaperDelivery)==(house: NAT & paper: NEWSPAPERS);
  List_Precondition(Machine(DeliveryController),requestPaperDelivery)==(house: NAT & paper: NEWSPAPERS);
  List_Precondition(Machine(DeliveryController),cancelPaperDelivery)==(house: available_houses);
  List_Precondition(Machine(DeliveryController),requestMagazineDelivery)==(house: available_houses & magazine: MAGAZINES)
END
&
THEORY ListSubstitutionX IS
  Expanded_List_Substitution(Machine(DeliveryController),requestMagazineDelivery)==(house: available_houses & magazine: MAGAZINES | house: dom(house_newspaper) ==> (house: available_houses & magazine: MAGAZINES | house|->magazine: house_magazine ==> ret:=HouseAlreadyDelivering [] not(house|->magazine: house_magazine) ==> (card({house}<|house_magazine)>=3 ==> ret:=MagazineLimitReached [] not(card({house}<|house_magazine)>=3) ==> house_magazine,ret:=house_magazine\/{house|->magazine},Success)) [] not(house: dom(house_newspaper)) ==> ret:=HouseNoNewspaper);
  Expanded_List_Substitution(Machine(DeliveryController),cancelPaperDelivery)==(house: available_houses | house: dom(house_magazine) ==> ret:=HouseHasMagazines [] not(house: dom(house_magazine)) ==> (house: available_houses & ret: MESSAGES | house: dom(house_newspaper) ==> house_newspaper,ret:={house}<<|house_newspaper,Success [] not(house: dom(house_newspaper)) ==> ret:=HouseNotDelivering));
  List_Substitution(Machine(DeliveryController),magazinesSold)==(ret:=card(house_magazine|>{magazine}));
  Expanded_List_Substitution(Machine(DeliveryController),magazinesSold)==(magazine: MAGAZINES | ret:=card(house_magazine|>{magazine}));
  List_Substitution(Machine(DeliveryController),whatMagazinesSold)==(ret:=ran(house_magazine));
  Expanded_List_Substitution(Machine(DeliveryController),whatMagazinesSold)==(btrue | ret:=ran(house_magazine));
  List_Substitution(Machine(DeliveryController),whatMagazinesDelivered)==(ret:=ran({house}<|house_magazine));
  Expanded_List_Substitution(Machine(DeliveryController),whatMagazinesDelivered)==(house: available_houses | ret:=ran({house}<|house_magazine));
  List_Substitution(Machine(DeliveryController),cancelMagazineDelivery)==(IF house|->magazine: house_magazine THEN house_magazine:=house_magazine-{house|->magazine} || ret:=Success ELSE ret:=HouseNotDelivering END);
  Expanded_List_Substitution(Machine(DeliveryController),cancelMagazineDelivery)==(house: available_houses & magazine: MAGAZINES | house|->magazine: house_magazine ==> house_magazine,ret:=house_magazine-{house|->magazine},Success [] not(house|->magazine: house_magazine) ==> ret:=HouseNotDelivering);
  List_Substitution(Machine(DeliveryController),papersSold)==(ret:=card(house_newspaper|>{paper}));
  Expanded_List_Substitution(Machine(DeliveryController),papersSold)==(paper: NEWSPAPERS | ret:=card(house_newspaper|>{paper}));
  List_Substitution(Machine(DeliveryController),whatPapersSold)==(ret:=ran(house_newspaper));
  Expanded_List_Substitution(Machine(DeliveryController),whatPapersSold)==(btrue | ret:=ran(house_newspaper));
  List_Substitution(Machine(DeliveryController),whatPaperDelivered)==(ret:=house_newspaper(house));
  Expanded_List_Substitution(Machine(DeliveryController),whatPaperDelivered)==(house: available_houses & house: dom(house_newspaper) | ret:=house_newspaper(house));
  List_Substitution(Machine(DeliveryController),housesDeliveredTo)==(ret:=dom(house_newspaper));
  Expanded_List_Substitution(Machine(DeliveryController),housesDeliveredTo)==(btrue | ret:=dom(house_newspaper));
  List_Substitution(Machine(DeliveryController),requestPaperDelivery)==(IF house: dom(house_newspaper) THEN ret:=NewspaperLimitReached ELSE house_newspaper:=house_newspaper\/{house|->paper} || ret:=Success END);
  Expanded_List_Substitution(Machine(DeliveryController),requestPaperDelivery)==(house: NAT & paper: NEWSPAPERS | house: dom(house_newspaper) ==> ret:=NewspaperLimitReached [] not(house: dom(house_newspaper)) ==> house_newspaper,ret:=house_newspaper\/{house|->paper},Success);
  List_Substitution(Machine(DeliveryController),cancelPaperDelivery)==(IF house: dom(house_magazine) THEN ret:=HouseHasMagazines ELSE ret <-- newspaper_cancelPaperDelivery(house) END);
  List_Substitution(Machine(DeliveryController),requestMagazineDelivery)==(IF house: dom(house_newspaper) THEN ret <-- magazine_requestMagazineDelivery(house,magazine) ELSE ret:=HouseNoNewspaper END)
END
&
THEORY ListConstantsX IS
  List_Valuable_Constants(Machine(DeliveryController))==(?);
  Inherited_List_Constants(Machine(DeliveryController))==(?);
  List_Constants(Machine(DeliveryController))==(?)
END
&
THEORY ListSetsX IS
  Set_Definition(Machine(DeliveryController),MAGAZINES)==({NewScientist,PrivateEye,Beano,RadioTimes,Vogue,Hello});
  Context_List_Enumerated(Machine(DeliveryController))==(MESSAGES);
  Context_List_Defered(Machine(DeliveryController))==(?);
  Context_List_Sets(Machine(DeliveryController))==(MESSAGES);
  List_Valuable_Sets(Machine(DeliveryController))==(?);
  Inherited_List_Enumerated(Machine(DeliveryController))==(NEWSPAPERS,MAGAZINES);
  Inherited_List_Defered(Machine(DeliveryController))==(?);
  Inherited_List_Sets(Machine(DeliveryController))==(NEWSPAPERS,MAGAZINES);
  List_Enumerated(Machine(DeliveryController))==(?);
  List_Defered(Machine(DeliveryController))==(?);
  List_Sets(Machine(DeliveryController))==(?);
  Set_Definition(Machine(DeliveryController),NEWSPAPERS)==({Guardian,Independent,Telegraph,Times,FinancialTimes});
  Set_Definition(Machine(DeliveryController),MESSAGES)==({Success,HouseAlreadyDelivering,HouseNotDelivering,HouseHasMagazines,HouseNoNewspaper,NewspaperLimitReached,MagazineLimitReached})
END
&
THEORY ListHiddenConstantsX IS
  Abstract_List_HiddenConstants(Machine(DeliveryController))==(?);
  Expanded_List_HiddenConstants(Machine(DeliveryController))==(?);
  List_HiddenConstants(Machine(DeliveryController))==(?);
  External_List_HiddenConstants(Machine(DeliveryController))==(?)
END
&
THEORY ListPropertiesX IS
  Abstract_List_Properties(Machine(DeliveryController))==(btrue);
  Context_List_Properties(Machine(DeliveryController))==(available_houses = 1..15 & MESSAGES: FIN(INTEGER) & not(MESSAGES = {}));
  Inherited_List_Properties(Machine(DeliveryController))==(NEWSPAPERS: FIN(INTEGER) & not(NEWSPAPERS = {}) & MAGAZINES: FIN(INTEGER) & not(MAGAZINES = {}));
  List_Properties(Machine(DeliveryController))==(btrue)
END
&
THEORY ListSeenInfoX IS
  Seen_Internal_List_Operations(Machine(DeliveryController),Machine(Shared))==(?);
  Seen_Context_List_Enumerated(Machine(DeliveryController))==(?);
  Seen_Context_List_Invariant(Machine(DeliveryController))==(btrue);
  Seen_Context_List_Assertions(Machine(DeliveryController))==(btrue);
  Seen_Context_List_Properties(Machine(DeliveryController))==(btrue);
  Seen_List_Constraints(Machine(DeliveryController))==(btrue);
  Seen_List_Operations(Machine(DeliveryController),Machine(Shared))==(?);
  Seen_Expanded_List_Invariant(Machine(DeliveryController),Machine(Shared))==(btrue)
END
&
THEORY ListANYVarX IS
  List_ANY_Var(Machine(DeliveryController),magazinesSold)==(?);
  List_ANY_Var(Machine(DeliveryController),whatMagazinesSold)==(?);
  List_ANY_Var(Machine(DeliveryController),whatMagazinesDelivered)==(?);
  List_ANY_Var(Machine(DeliveryController),cancelMagazineDelivery)==(?);
  List_ANY_Var(Machine(DeliveryController),papersSold)==(?);
  List_ANY_Var(Machine(DeliveryController),whatPapersSold)==(?);
  List_ANY_Var(Machine(DeliveryController),whatPaperDelivered)==(?);
  List_ANY_Var(Machine(DeliveryController),housesDeliveredTo)==(?);
  List_ANY_Var(Machine(DeliveryController),requestPaperDelivery)==(?);
  List_ANY_Var(Machine(DeliveryController),cancelPaperDelivery)==(?);
  List_ANY_Var(Machine(DeliveryController),requestMagazineDelivery)==(?)
END
&
THEORY ListOfIdsX IS
  List_Of_Ids(Machine(DeliveryController)) == (? | MAGAZINES,NewScientist,PrivateEye,Beano,RadioTimes,Vogue,Hello,NEWSPAPERS,Guardian,Independent,Telegraph,Times,FinancialTimes | ? | V,house_magazine,house_newspaper | cancelPaperDelivery,requestMagazineDelivery | requestPaperDelivery,housesDeliveredTo,whatPaperDelivered,whatPapersSold,papersSold,cancelMagazineDelivery,whatMagazinesDelivered,whatMagazinesSold,magazinesSold | seen(Machine(Shared)),included(Machine(Newspaper)),included(Machine(Magazine)) | ? | DeliveryController);
  List_Of_HiddenCst_Ids(Machine(DeliveryController)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(DeliveryController)) == (?);
  List_Of_VisibleVar_Ids(Machine(DeliveryController)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(DeliveryController)) == (seen(Machine(Shared)): (available_houses,MESSAGES,Success,HouseAlreadyDelivering,HouseNotDelivering,HouseHasMagazines,HouseNoNewspaper,NewspaperLimitReached,MagazineLimitReached | ? | ? | ? | ? | ? | ? | ? | ?));
  List_Of_Ids(Machine(Magazine)) == (MAGAZINES,NewScientist,PrivateEye,Beano,RadioTimes,Vogue,Hello | ? | house_magazine | ? | magazine_requestMagazineDelivery,cancelMagazineDelivery,whatMagazinesDelivered,whatMagazinesSold,magazinesSold | ? | seen(Machine(Shared)) | ? | Magazine);
  List_Of_HiddenCst_Ids(Machine(Magazine)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Magazine)) == (?);
  List_Of_VisibleVar_Ids(Machine(Magazine)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Magazine)) == (?: ?);
  List_Of_Ids(Machine(Shared)) == (available_houses,MESSAGES,Success,HouseAlreadyDelivering,HouseNotDelivering,HouseHasMagazines,HouseNoNewspaper,NewspaperLimitReached,MagazineLimitReached | ? | ? | ? | ? | ? | ? | ? | Shared);
  List_Of_HiddenCst_Ids(Machine(Shared)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Shared)) == (available_houses);
  List_Of_VisibleVar_Ids(Machine(Shared)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Shared)) == (?: ?);
  List_Of_Ids(Machine(Newspaper)) == (NEWSPAPERS,Guardian,Independent,Telegraph,Times,FinancialTimes | ? | house_newspaper | ? | requestPaperDelivery,newspaper_cancelPaperDelivery,housesDeliveredTo,whatPaperDelivered,whatPapersSold,papersSold | ? | seen(Machine(Shared)) | ? | Newspaper);
  List_Of_HiddenCst_Ids(Machine(Newspaper)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Newspaper)) == (?);
  List_Of_VisibleVar_Ids(Machine(Newspaper)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Newspaper)) == (?: ?)
END
&
THEORY SetsEnvX IS
  Sets(Machine(DeliveryController)) == (Type(NEWSPAPERS) == Cst(SetOf(etype(NEWSPAPERS,0,4)));Type(MAGAZINES) == Cst(SetOf(etype(MAGAZINES,0,5))))
END
&
THEORY ConstantsEnvX IS
  Constants(Machine(DeliveryController)) == (Type(FinancialTimes) == Cst(etype(NEWSPAPERS,0,4));Type(Times) == Cst(etype(NEWSPAPERS,0,4));Type(Telegraph) == Cst(etype(NEWSPAPERS,0,4));Type(Independent) == Cst(etype(NEWSPAPERS,0,4));Type(Guardian) == Cst(etype(NEWSPAPERS,0,4));Type(Hello) == Cst(etype(MAGAZINES,0,5));Type(Vogue) == Cst(etype(MAGAZINES,0,5));Type(RadioTimes) == Cst(etype(MAGAZINES,0,5));Type(Beano) == Cst(etype(MAGAZINES,0,5));Type(PrivateEye) == Cst(etype(MAGAZINES,0,5));Type(NewScientist) == Cst(etype(MAGAZINES,0,5)))
END
&
THEORY VariablesEnvX IS
  Variables(Machine(DeliveryController)) == (Type(house_newspaper) == Mvl(SetOf(btype(INTEGER,?,?)*etype(NEWSPAPERS,?,?)));Type(house_magazine) == Mvl(SetOf(btype(INTEGER,?,?)*etype(MAGAZINES,?,?))))
END
&
THEORY OperationsEnvX IS
  Operations(Machine(DeliveryController)) == (Type(requestPaperDelivery) == Cst(etype(MESSAGES,?,?),btype(INTEGER,?,?)*etype(NEWSPAPERS,?,?));Type(housesDeliveredTo) == Cst(SetOf(btype(INTEGER,?,?)),No_type);Type(whatPaperDelivered) == Cst(etype(NEWSPAPERS,?,?),btype(INTEGER,?,?));Type(whatPapersSold) == Cst(SetOf(etype(NEWSPAPERS,?,?)),No_type);Type(papersSold) == Cst(btype(INTEGER,?,?),etype(NEWSPAPERS,?,?));Type(cancelMagazineDelivery) == Cst(etype(MESSAGES,?,?),btype(INTEGER,?,?)*etype(MAGAZINES,?,?));Type(whatMagazinesDelivered) == Cst(SetOf(etype(MAGAZINES,?,?)),btype(INTEGER,?,?));Type(whatMagazinesSold) == Cst(SetOf(etype(MAGAZINES,?,?)),No_type);Type(magazinesSold) == Cst(btype(INTEGER,?,?),etype(MAGAZINES,?,?));Type(requestMagazineDelivery) == Cst(etype(MESSAGES,?,?),btype(INTEGER,?,?)*etype(MAGAZINES,?,?));Type(cancelPaperDelivery) == Cst(etype(MESSAGES,?,?),btype(INTEGER,?,?)));
  Observers(Machine(DeliveryController)) == (Type(housesDeliveredTo) == Cst(SetOf(btype(INTEGER,?,?)),No_type);Type(whatPaperDelivered) == Cst(etype(NEWSPAPERS,?,?),btype(INTEGER,?,?));Type(whatPapersSold) == Cst(SetOf(etype(NEWSPAPERS,?,?)),No_type);Type(papersSold) == Cst(btype(INTEGER,?,?),etype(NEWSPAPERS,?,?));Type(whatMagazinesDelivered) == Cst(SetOf(etype(MAGAZINES,?,?)),btype(INTEGER,?,?));Type(whatMagazinesSold) == Cst(SetOf(etype(MAGAZINES,?,?)),No_type);Type(magazinesSold) == Cst(btype(INTEGER,?,?),etype(MAGAZINES,?,?));Type(requestMagazineDelivery) == Cst(etype(MESSAGES,?,?),btype(INTEGER,?,?)*etype(MAGAZINES,?,?));Type(cancelPaperDelivery) == Cst(etype(MESSAGES,?,?),btype(INTEGER,?,?)))
END
&
THEORY TCIntRdX IS
  predB0 == OK;
  extended_sees == KO;
  B0check_tab == KO;
  local_op == OK;
  abstract_constants_visible_in_values == KO;
  project_type == SOFTWARE_TYPE;
  event_b_deadlockfreeness == KO;
  variant_clause_mandatory == KO;
  event_b_coverage == KO;
  event_b_exclusivity == KO;
  genFeasibilityPO == KO
END
)
