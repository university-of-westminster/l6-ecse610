﻿Normalised(
THEORY MagicNumberX IS
  MagicNumber(Machine(Magazine))==(3.5)
END
&
THEORY UpperLevelX IS
  First_Level(Machine(Magazine))==(Machine(Magazine));
  Level(Machine(Magazine))==(0)
END
&
THEORY LoadedStructureX IS
  Machine(Magazine)
END
&
THEORY ListSeesX IS
  List_Sees(Machine(Magazine))==(Shared)
END
&
THEORY ListUsesX IS
  List_Uses(Machine(Magazine))==(?)
END
&
THEORY ListIncludesX IS
  Inherited_List_Includes(Machine(Magazine))==(?);
  List_Includes(Machine(Magazine))==(?)
END
&
THEORY ListPromotesX IS
  List_Promotes(Machine(Magazine))==(?)
END
&
THEORY ListExtendsX IS
  List_Extends(Machine(Magazine))==(?)
END
&
THEORY ListVariablesX IS
  External_Context_List_Variables(Machine(Magazine))==(?);
  Context_List_Variables(Machine(Magazine))==(?);
  Abstract_List_Variables(Machine(Magazine))==(?);
  Local_List_Variables(Machine(Magazine))==(house_magazine);
  List_Variables(Machine(Magazine))==(house_magazine);
  External_List_Variables(Machine(Magazine))==(house_magazine)
END
&
THEORY ListVisibleVariablesX IS
  Inherited_List_VisibleVariables(Machine(Magazine))==(?);
  Abstract_List_VisibleVariables(Machine(Magazine))==(?);
  External_List_VisibleVariables(Machine(Magazine))==(?);
  Expanded_List_VisibleVariables(Machine(Magazine))==(?);
  List_VisibleVariables(Machine(Magazine))==(?);
  Internal_List_VisibleVariables(Machine(Magazine))==(?)
END
&
THEORY ListInvariantX IS
  Gluing_Seen_List_Invariant(Machine(Magazine))==(btrue);
  Gluing_List_Invariant(Machine(Magazine))==(btrue);
  Expanded_List_Invariant(Machine(Magazine))==(btrue);
  Abstract_List_Invariant(Machine(Magazine))==(btrue);
  Context_List_Invariant(Machine(Magazine))==(btrue);
  List_Invariant(Machine(Magazine))==(house_magazine: available_houses <-> MAGAZINES)
END
&
THEORY ListAssertionsX IS
  Expanded_List_Assertions(Machine(Magazine))==(btrue);
  Abstract_List_Assertions(Machine(Magazine))==(btrue);
  Context_List_Assertions(Machine(Magazine))==(btrue);
  List_Assertions(Machine(Magazine))==(btrue)
END
&
THEORY ListCoverageX IS
  List_Coverage(Machine(Magazine))==(btrue)
END
&
THEORY ListExclusivityX IS
  List_Exclusivity(Machine(Magazine))==(btrue)
END
&
THEORY ListInitialisationX IS
  Expanded_List_Initialisation(Machine(Magazine))==(house_magazine:={});
  Context_List_Initialisation(Machine(Magazine))==(skip);
  List_Initialisation(Machine(Magazine))==(house_magazine:={})
END
&
THEORY ListParametersX IS
  List_Parameters(Machine(Magazine))==(?)
END
&
THEORY ListInstanciatedParametersX IS
  List_Instanciated_Parameters(Machine(Magazine),Machine(Shared))==(?)
END
&
THEORY ListConstraintsX IS
  List_Context_Constraints(Machine(Magazine))==(btrue);
  List_Constraints(Machine(Magazine))==(btrue)
END
&
THEORY ListOperationsX IS
  Internal_List_Operations(Machine(Magazine))==(magazine_requestMagazineDelivery,cancelMagazineDelivery,whatMagazinesDelivered,whatMagazinesSold,magazinesSold);
  List_Operations(Machine(Magazine))==(magazine_requestMagazineDelivery,cancelMagazineDelivery,whatMagazinesDelivered,whatMagazinesSold,magazinesSold)
END
&
THEORY ListInputX IS
  List_Input(Machine(Magazine),magazine_requestMagazineDelivery)==(house,magazine);
  List_Input(Machine(Magazine),cancelMagazineDelivery)==(house,magazine);
  List_Input(Machine(Magazine),whatMagazinesDelivered)==(house);
  List_Input(Machine(Magazine),whatMagazinesSold)==(?);
  List_Input(Machine(Magazine),magazinesSold)==(magazine)
END
&
THEORY ListOutputX IS
  List_Output(Machine(Magazine),magazine_requestMagazineDelivery)==(ret);
  List_Output(Machine(Magazine),cancelMagazineDelivery)==(ret);
  List_Output(Machine(Magazine),whatMagazinesDelivered)==(ret);
  List_Output(Machine(Magazine),whatMagazinesSold)==(ret);
  List_Output(Machine(Magazine),magazinesSold)==(ret)
END
&
THEORY ListHeaderX IS
  List_Header(Machine(Magazine),magazine_requestMagazineDelivery)==(ret <-- magazine_requestMagazineDelivery(house,magazine));
  List_Header(Machine(Magazine),cancelMagazineDelivery)==(ret <-- cancelMagazineDelivery(house,magazine));
  List_Header(Machine(Magazine),whatMagazinesDelivered)==(ret <-- whatMagazinesDelivered(house));
  List_Header(Machine(Magazine),whatMagazinesSold)==(ret <-- whatMagazinesSold);
  List_Header(Machine(Magazine),magazinesSold)==(ret <-- magazinesSold(magazine))
END
&
THEORY ListOperationGuardX END
&
THEORY ListPreconditionX IS
  List_Precondition(Machine(Magazine),magazine_requestMagazineDelivery)==(house: available_houses & magazine: MAGAZINES);
  List_Precondition(Machine(Magazine),cancelMagazineDelivery)==(house: available_houses & magazine: MAGAZINES);
  List_Precondition(Machine(Magazine),whatMagazinesDelivered)==(house: available_houses);
  List_Precondition(Machine(Magazine),whatMagazinesSold)==(btrue);
  List_Precondition(Machine(Magazine),magazinesSold)==(magazine: MAGAZINES)
END
&
THEORY ListSubstitutionX IS
  Expanded_List_Substitution(Machine(Magazine),magazinesSold)==(magazine: MAGAZINES | ret:=card(house_magazine|>{magazine}));
  Expanded_List_Substitution(Machine(Magazine),whatMagazinesSold)==(btrue | ret:=ran(house_magazine));
  Expanded_List_Substitution(Machine(Magazine),whatMagazinesDelivered)==(house: available_houses | ret:=ran({house}<|house_magazine));
  Expanded_List_Substitution(Machine(Magazine),cancelMagazineDelivery)==(house: available_houses & magazine: MAGAZINES | house|->magazine: house_magazine ==> house_magazine,ret:=house_magazine-{house|->magazine},Success [] not(house|->magazine: house_magazine) ==> ret:=HouseNotDelivering);
  Expanded_List_Substitution(Machine(Magazine),magazine_requestMagazineDelivery)==(house: available_houses & magazine: MAGAZINES | house|->magazine: house_magazine ==> ret:=HouseAlreadyDelivering [] not(house|->magazine: house_magazine) ==> (card({house}<|house_magazine)>=3 ==> ret:=MagazineLimitReached [] not(card({house}<|house_magazine)>=3) ==> house_magazine,ret:=house_magazine\/{house|->magazine},Success));
  List_Substitution(Machine(Magazine),magazine_requestMagazineDelivery)==(IF house|->magazine: house_magazine THEN ret:=HouseAlreadyDelivering ELSIF card({house}<|house_magazine)>=3 THEN ret:=MagazineLimitReached ELSE house_magazine:=house_magazine\/{house|->magazine} || ret:=Success END);
  List_Substitution(Machine(Magazine),cancelMagazineDelivery)==(IF house|->magazine: house_magazine THEN house_magazine:=house_magazine-{house|->magazine} || ret:=Success ELSE ret:=HouseNotDelivering END);
  List_Substitution(Machine(Magazine),whatMagazinesDelivered)==(ret:=ran({house}<|house_magazine));
  List_Substitution(Machine(Magazine),whatMagazinesSold)==(ret:=ran(house_magazine));
  List_Substitution(Machine(Magazine),magazinesSold)==(ret:=card(house_magazine|>{magazine}))
END
&
THEORY ListConstantsX IS
  List_Valuable_Constants(Machine(Magazine))==(?);
  Inherited_List_Constants(Machine(Magazine))==(?);
  List_Constants(Machine(Magazine))==(?)
END
&
THEORY ListSetsX IS
  Set_Definition(Machine(Magazine),MESSAGES)==({Success,HouseAlreadyDelivering,HouseNotDelivering,HouseHasMagazines,HouseNoNewspaper,NewspaperLimitReached,MagazineLimitReached});
  Context_List_Enumerated(Machine(Magazine))==(MESSAGES);
  Context_List_Defered(Machine(Magazine))==(?);
  Context_List_Sets(Machine(Magazine))==(MESSAGES);
  List_Valuable_Sets(Machine(Magazine))==(?);
  Inherited_List_Enumerated(Machine(Magazine))==(?);
  Inherited_List_Defered(Machine(Magazine))==(?);
  Inherited_List_Sets(Machine(Magazine))==(?);
  List_Enumerated(Machine(Magazine))==(MAGAZINES);
  List_Defered(Machine(Magazine))==(?);
  List_Sets(Machine(Magazine))==(MAGAZINES);
  Set_Definition(Machine(Magazine),MAGAZINES)==({NewScientist,PrivateEye,Beano,RadioTimes,Vogue,Hello})
END
&
THEORY ListHiddenConstantsX IS
  Abstract_List_HiddenConstants(Machine(Magazine))==(?);
  Expanded_List_HiddenConstants(Machine(Magazine))==(?);
  List_HiddenConstants(Machine(Magazine))==(?);
  External_List_HiddenConstants(Machine(Magazine))==(?)
END
&
THEORY ListPropertiesX IS
  Abstract_List_Properties(Machine(Magazine))==(btrue);
  Context_List_Properties(Machine(Magazine))==(available_houses = 1..15 & MESSAGES: FIN(INTEGER) & not(MESSAGES = {}));
  Inherited_List_Properties(Machine(Magazine))==(btrue);
  List_Properties(Machine(Magazine))==(MAGAZINES: FIN(INTEGER) & not(MAGAZINES = {}))
END
&
THEORY ListSeenInfoX IS
  Seen_Internal_List_Operations(Machine(Magazine),Machine(Shared))==(?);
  Seen_Context_List_Enumerated(Machine(Magazine))==(?);
  Seen_Context_List_Invariant(Machine(Magazine))==(btrue);
  Seen_Context_List_Assertions(Machine(Magazine))==(btrue);
  Seen_Context_List_Properties(Machine(Magazine))==(btrue);
  Seen_List_Constraints(Machine(Magazine))==(btrue);
  Seen_List_Operations(Machine(Magazine),Machine(Shared))==(?);
  Seen_Expanded_List_Invariant(Machine(Magazine),Machine(Shared))==(btrue)
END
&
THEORY ListANYVarX IS
  List_ANY_Var(Machine(Magazine),magazine_requestMagazineDelivery)==(?);
  List_ANY_Var(Machine(Magazine),cancelMagazineDelivery)==(?);
  List_ANY_Var(Machine(Magazine),whatMagazinesDelivered)==(?);
  List_ANY_Var(Machine(Magazine),whatMagazinesSold)==(?);
  List_ANY_Var(Machine(Magazine),magazinesSold)==(?)
END
&
THEORY ListOfIdsX IS
  List_Of_Ids(Machine(Magazine)) == (MAGAZINES,NewScientist,PrivateEye,Beano,RadioTimes,Vogue,Hello | ? | house_magazine | ? | magazine_requestMagazineDelivery,cancelMagazineDelivery,whatMagazinesDelivered,whatMagazinesSold,magazinesSold | ? | seen(Machine(Shared)) | ? | Magazine);
  List_Of_HiddenCst_Ids(Machine(Magazine)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Magazine)) == (?);
  List_Of_VisibleVar_Ids(Machine(Magazine)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Magazine)) == (?: ?);
  List_Of_Ids(Machine(Shared)) == (available_houses,MESSAGES,Success,HouseAlreadyDelivering,HouseNotDelivering,HouseHasMagazines,HouseNoNewspaper,NewspaperLimitReached,MagazineLimitReached | ? | ? | ? | ? | ? | ? | ? | Shared);
  List_Of_HiddenCst_Ids(Machine(Shared)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Shared)) == (available_houses);
  List_Of_VisibleVar_Ids(Machine(Shared)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Shared)) == (?: ?)
END
&
THEORY SetsEnvX IS
  Sets(Machine(Magazine)) == (Type(MAGAZINES) == Cst(SetOf(etype(MAGAZINES,0,5))))
END
&
THEORY ConstantsEnvX IS
  Constants(Machine(Magazine)) == (Type(NewScientist) == Cst(etype(MAGAZINES,0,5));Type(PrivateEye) == Cst(etype(MAGAZINES,0,5));Type(Beano) == Cst(etype(MAGAZINES,0,5));Type(RadioTimes) == Cst(etype(MAGAZINES,0,5));Type(Vogue) == Cst(etype(MAGAZINES,0,5));Type(Hello) == Cst(etype(MAGAZINES,0,5)))
END
&
THEORY VariablesEnvX IS
  Variables(Machine(Magazine)) == (Type(house_magazine) == Mvl(SetOf(btype(INTEGER,?,?)*etype(MAGAZINES,?,?))))
END
&
THEORY OperationsEnvX IS
  Operations(Machine(Magazine)) == (Type(magazinesSold) == Cst(btype(INTEGER,?,?),etype(MAGAZINES,?,?));Type(whatMagazinesSold) == Cst(SetOf(etype(MAGAZINES,?,?)),No_type);Type(whatMagazinesDelivered) == Cst(SetOf(etype(MAGAZINES,?,?)),btype(INTEGER,?,?));Type(cancelMagazineDelivery) == Cst(etype(MESSAGES,?,?),btype(INTEGER,?,?)*etype(MAGAZINES,?,?));Type(magazine_requestMagazineDelivery) == Cst(etype(MESSAGES,?,?),btype(INTEGER,?,?)*etype(MAGAZINES,?,?)));
  Observers(Machine(Magazine)) == (Type(magazinesSold) == Cst(btype(INTEGER,?,?),etype(MAGAZINES,?,?));Type(whatMagazinesSold) == Cst(SetOf(etype(MAGAZINES,?,?)),No_type);Type(whatMagazinesDelivered) == Cst(SetOf(etype(MAGAZINES,?,?)),btype(INTEGER,?,?)))
END
&
THEORY TCIntRdX IS
  predB0 == OK;
  extended_sees == KO;
  B0check_tab == KO;
  local_op == OK;
  abstract_constants_visible_in_values == KO;
  project_type == SOFTWARE_TYPE;
  event_b_deadlockfreeness == KO;
  variant_clause_mandatory == KO;
  event_b_coverage == KO;
  event_b_exclusivity == KO;
  genFeasibilityPO == KO
END
)
