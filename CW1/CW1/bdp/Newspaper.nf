﻿Normalised(
THEORY MagicNumberX IS
  MagicNumber(Machine(Newspaper))==(3.5)
END
&
THEORY UpperLevelX IS
  First_Level(Machine(Newspaper))==(Machine(Newspaper));
  Level(Machine(Newspaper))==(0)
END
&
THEORY LoadedStructureX IS
  Machine(Newspaper)
END
&
THEORY ListSeesX IS
  List_Sees(Machine(Newspaper))==(Shared)
END
&
THEORY ListUsesX IS
  List_Uses(Machine(Newspaper))==(?)
END
&
THEORY ListIncludesX IS
  Inherited_List_Includes(Machine(Newspaper))==(?);
  List_Includes(Machine(Newspaper))==(?)
END
&
THEORY ListPromotesX IS
  List_Promotes(Machine(Newspaper))==(?)
END
&
THEORY ListExtendsX IS
  List_Extends(Machine(Newspaper))==(?)
END
&
THEORY ListVariablesX IS
  External_Context_List_Variables(Machine(Newspaper))==(?);
  Context_List_Variables(Machine(Newspaper))==(?);
  Abstract_List_Variables(Machine(Newspaper))==(?);
  Local_List_Variables(Machine(Newspaper))==(house_newspaper);
  List_Variables(Machine(Newspaper))==(house_newspaper);
  External_List_Variables(Machine(Newspaper))==(house_newspaper)
END
&
THEORY ListVisibleVariablesX IS
  Inherited_List_VisibleVariables(Machine(Newspaper))==(?);
  Abstract_List_VisibleVariables(Machine(Newspaper))==(?);
  External_List_VisibleVariables(Machine(Newspaper))==(?);
  Expanded_List_VisibleVariables(Machine(Newspaper))==(?);
  List_VisibleVariables(Machine(Newspaper))==(?);
  Internal_List_VisibleVariables(Machine(Newspaper))==(?)
END
&
THEORY ListInvariantX IS
  Gluing_Seen_List_Invariant(Machine(Newspaper))==(btrue);
  Gluing_List_Invariant(Machine(Newspaper))==(btrue);
  Expanded_List_Invariant(Machine(Newspaper))==(btrue);
  Abstract_List_Invariant(Machine(Newspaper))==(btrue);
  Context_List_Invariant(Machine(Newspaper))==(btrue);
  List_Invariant(Machine(Newspaper))==(house_newspaper: available_houses +-> NEWSPAPERS)
END
&
THEORY ListAssertionsX IS
  Expanded_List_Assertions(Machine(Newspaper))==(btrue);
  Abstract_List_Assertions(Machine(Newspaper))==(btrue);
  Context_List_Assertions(Machine(Newspaper))==(btrue);
  List_Assertions(Machine(Newspaper))==(btrue)
END
&
THEORY ListCoverageX IS
  List_Coverage(Machine(Newspaper))==(btrue)
END
&
THEORY ListExclusivityX IS
  List_Exclusivity(Machine(Newspaper))==(btrue)
END
&
THEORY ListInitialisationX IS
  Expanded_List_Initialisation(Machine(Newspaper))==(house_newspaper:={});
  Context_List_Initialisation(Machine(Newspaper))==(skip);
  List_Initialisation(Machine(Newspaper))==(house_newspaper:={})
END
&
THEORY ListParametersX IS
  List_Parameters(Machine(Newspaper))==(?)
END
&
THEORY ListInstanciatedParametersX IS
  List_Instanciated_Parameters(Machine(Newspaper),Machine(Shared))==(?)
END
&
THEORY ListConstraintsX IS
  List_Context_Constraints(Machine(Newspaper))==(btrue);
  List_Constraints(Machine(Newspaper))==(btrue)
END
&
THEORY ListOperationsX IS
  Internal_List_Operations(Machine(Newspaper))==(requestPaperDelivery,newspaper_cancelPaperDelivery,housesDeliveredTo,whatPaperDelivered,whatPapersSold,papersSold);
  List_Operations(Machine(Newspaper))==(requestPaperDelivery,newspaper_cancelPaperDelivery,housesDeliveredTo,whatPaperDelivered,whatPapersSold,papersSold)
END
&
THEORY ListInputX IS
  List_Input(Machine(Newspaper),requestPaperDelivery)==(house,paper);
  List_Input(Machine(Newspaper),newspaper_cancelPaperDelivery)==(house);
  List_Input(Machine(Newspaper),housesDeliveredTo)==(?);
  List_Input(Machine(Newspaper),whatPaperDelivered)==(house);
  List_Input(Machine(Newspaper),whatPapersSold)==(?);
  List_Input(Machine(Newspaper),papersSold)==(paper)
END
&
THEORY ListOutputX IS
  List_Output(Machine(Newspaper),requestPaperDelivery)==(ret);
  List_Output(Machine(Newspaper),newspaper_cancelPaperDelivery)==(ret);
  List_Output(Machine(Newspaper),housesDeliveredTo)==(ret);
  List_Output(Machine(Newspaper),whatPaperDelivered)==(ret);
  List_Output(Machine(Newspaper),whatPapersSold)==(ret);
  List_Output(Machine(Newspaper),papersSold)==(ret)
END
&
THEORY ListHeaderX IS
  List_Header(Machine(Newspaper),requestPaperDelivery)==(ret <-- requestPaperDelivery(house,paper));
  List_Header(Machine(Newspaper),newspaper_cancelPaperDelivery)==(ret <-- newspaper_cancelPaperDelivery(house));
  List_Header(Machine(Newspaper),housesDeliveredTo)==(ret <-- housesDeliveredTo);
  List_Header(Machine(Newspaper),whatPaperDelivered)==(ret <-- whatPaperDelivered(house));
  List_Header(Machine(Newspaper),whatPapersSold)==(ret <-- whatPapersSold);
  List_Header(Machine(Newspaper),papersSold)==(ret <-- papersSold(paper))
END
&
THEORY ListOperationGuardX END
&
THEORY ListPreconditionX IS
  List_Precondition(Machine(Newspaper),requestPaperDelivery)==(house: NAT & paper: NEWSPAPERS);
  List_Precondition(Machine(Newspaper),newspaper_cancelPaperDelivery)==(house: available_houses & ret: MESSAGES);
  List_Precondition(Machine(Newspaper),housesDeliveredTo)==(btrue);
  List_Precondition(Machine(Newspaper),whatPaperDelivered)==(house: available_houses & house: dom(house_newspaper));
  List_Precondition(Machine(Newspaper),whatPapersSold)==(btrue);
  List_Precondition(Machine(Newspaper),papersSold)==(paper: NEWSPAPERS)
END
&
THEORY ListSubstitutionX IS
  Expanded_List_Substitution(Machine(Newspaper),papersSold)==(paper: NEWSPAPERS | ret:=card(house_newspaper|>{paper}));
  Expanded_List_Substitution(Machine(Newspaper),whatPapersSold)==(btrue | ret:=ran(house_newspaper));
  Expanded_List_Substitution(Machine(Newspaper),whatPaperDelivered)==(house: available_houses & house: dom(house_newspaper) | ret:=house_newspaper(house));
  Expanded_List_Substitution(Machine(Newspaper),housesDeliveredTo)==(btrue | ret:=dom(house_newspaper));
  Expanded_List_Substitution(Machine(Newspaper),newspaper_cancelPaperDelivery)==(house: available_houses & ret: MESSAGES | house: dom(house_newspaper) ==> house_newspaper,ret:={house}<<|house_newspaper,Success [] not(house: dom(house_newspaper)) ==> ret:=HouseNotDelivering);
  Expanded_List_Substitution(Machine(Newspaper),requestPaperDelivery)==(house: NAT & paper: NEWSPAPERS | house: dom(house_newspaper) ==> ret:=NewspaperLimitReached [] not(house: dom(house_newspaper)) ==> house_newspaper,ret:=house_newspaper\/{house|->paper},Success);
  List_Substitution(Machine(Newspaper),requestPaperDelivery)==(IF house: dom(house_newspaper) THEN ret:=NewspaperLimitReached ELSE house_newspaper:=house_newspaper\/{house|->paper} || ret:=Success END);
  List_Substitution(Machine(Newspaper),newspaper_cancelPaperDelivery)==(IF house: dom(house_newspaper) THEN house_newspaper:={house}<<|house_newspaper || ret:=Success ELSE ret:=HouseNotDelivering END);
  List_Substitution(Machine(Newspaper),housesDeliveredTo)==(ret:=dom(house_newspaper));
  List_Substitution(Machine(Newspaper),whatPaperDelivered)==(ret:=house_newspaper(house));
  List_Substitution(Machine(Newspaper),whatPapersSold)==(ret:=ran(house_newspaper));
  List_Substitution(Machine(Newspaper),papersSold)==(ret:=card(house_newspaper|>{paper}))
END
&
THEORY ListConstantsX IS
  List_Valuable_Constants(Machine(Newspaper))==(?);
  Inherited_List_Constants(Machine(Newspaper))==(?);
  List_Constants(Machine(Newspaper))==(?)
END
&
THEORY ListSetsX IS
  Set_Definition(Machine(Newspaper),MESSAGES)==({Success,HouseAlreadyDelivering,HouseNotDelivering,HouseHasMagazines,HouseNoNewspaper,NewspaperLimitReached,MagazineLimitReached});
  Context_List_Enumerated(Machine(Newspaper))==(MESSAGES);
  Context_List_Defered(Machine(Newspaper))==(?);
  Context_List_Sets(Machine(Newspaper))==(MESSAGES);
  List_Valuable_Sets(Machine(Newspaper))==(?);
  Inherited_List_Enumerated(Machine(Newspaper))==(?);
  Inherited_List_Defered(Machine(Newspaper))==(?);
  Inherited_List_Sets(Machine(Newspaper))==(?);
  List_Enumerated(Machine(Newspaper))==(NEWSPAPERS);
  List_Defered(Machine(Newspaper))==(?);
  List_Sets(Machine(Newspaper))==(NEWSPAPERS);
  Set_Definition(Machine(Newspaper),NEWSPAPERS)==({Guardian,Independent,Telegraph,Times,FinancialTimes})
END
&
THEORY ListHiddenConstantsX IS
  Abstract_List_HiddenConstants(Machine(Newspaper))==(?);
  Expanded_List_HiddenConstants(Machine(Newspaper))==(?);
  List_HiddenConstants(Machine(Newspaper))==(?);
  External_List_HiddenConstants(Machine(Newspaper))==(?)
END
&
THEORY ListPropertiesX IS
  Abstract_List_Properties(Machine(Newspaper))==(btrue);
  Context_List_Properties(Machine(Newspaper))==(available_houses = 1..15 & MESSAGES: FIN(INTEGER) & not(MESSAGES = {}));
  Inherited_List_Properties(Machine(Newspaper))==(btrue);
  List_Properties(Machine(Newspaper))==(NEWSPAPERS: FIN(INTEGER) & not(NEWSPAPERS = {}))
END
&
THEORY ListSeenInfoX IS
  Seen_Internal_List_Operations(Machine(Newspaper),Machine(Shared))==(?);
  Seen_Context_List_Enumerated(Machine(Newspaper))==(?);
  Seen_Context_List_Invariant(Machine(Newspaper))==(btrue);
  Seen_Context_List_Assertions(Machine(Newspaper))==(btrue);
  Seen_Context_List_Properties(Machine(Newspaper))==(btrue);
  Seen_List_Constraints(Machine(Newspaper))==(btrue);
  Seen_List_Operations(Machine(Newspaper),Machine(Shared))==(?);
  Seen_Expanded_List_Invariant(Machine(Newspaper),Machine(Shared))==(btrue)
END
&
THEORY ListANYVarX IS
  List_ANY_Var(Machine(Newspaper),requestPaperDelivery)==(?);
  List_ANY_Var(Machine(Newspaper),newspaper_cancelPaperDelivery)==(?);
  List_ANY_Var(Machine(Newspaper),housesDeliveredTo)==(?);
  List_ANY_Var(Machine(Newspaper),whatPaperDelivered)==(?);
  List_ANY_Var(Machine(Newspaper),whatPapersSold)==(?);
  List_ANY_Var(Machine(Newspaper),papersSold)==(?)
END
&
THEORY ListOfIdsX IS
  List_Of_Ids(Machine(Newspaper)) == (NEWSPAPERS,Guardian,Independent,Telegraph,Times,FinancialTimes | ? | house_newspaper | ? | requestPaperDelivery,newspaper_cancelPaperDelivery,housesDeliveredTo,whatPaperDelivered,whatPapersSold,papersSold | ? | seen(Machine(Shared)) | ? | Newspaper);
  List_Of_HiddenCst_Ids(Machine(Newspaper)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Newspaper)) == (?);
  List_Of_VisibleVar_Ids(Machine(Newspaper)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Newspaper)) == (?: ?);
  List_Of_Ids(Machine(Shared)) == (available_houses,MESSAGES,Success,HouseAlreadyDelivering,HouseNotDelivering,HouseHasMagazines,HouseNoNewspaper,NewspaperLimitReached,MagazineLimitReached | ? | ? | ? | ? | ? | ? | ? | Shared);
  List_Of_HiddenCst_Ids(Machine(Shared)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Shared)) == (available_houses);
  List_Of_VisibleVar_Ids(Machine(Shared)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Shared)) == (?: ?)
END
&
THEORY SetsEnvX IS
  Sets(Machine(Newspaper)) == (Type(NEWSPAPERS) == Cst(SetOf(etype(NEWSPAPERS,0,4))))
END
&
THEORY ConstantsEnvX IS
  Constants(Machine(Newspaper)) == (Type(Guardian) == Cst(etype(NEWSPAPERS,0,4));Type(Independent) == Cst(etype(NEWSPAPERS,0,4));Type(Telegraph) == Cst(etype(NEWSPAPERS,0,4));Type(Times) == Cst(etype(NEWSPAPERS,0,4));Type(FinancialTimes) == Cst(etype(NEWSPAPERS,0,4)))
END
&
THEORY VariablesEnvX IS
  Variables(Machine(Newspaper)) == (Type(house_newspaper) == Mvl(SetOf(btype(INTEGER,?,?)*etype(NEWSPAPERS,?,?))))
END
&
THEORY OperationsEnvX IS
  Operations(Machine(Newspaper)) == (Type(papersSold) == Cst(btype(INTEGER,?,?),etype(NEWSPAPERS,?,?));Type(whatPapersSold) == Cst(SetOf(etype(NEWSPAPERS,?,?)),No_type);Type(whatPaperDelivered) == Cst(etype(NEWSPAPERS,?,?),btype(INTEGER,?,?));Type(housesDeliveredTo) == Cst(SetOf(btype(INTEGER,?,?)),No_type);Type(newspaper_cancelPaperDelivery) == Cst(etype(MESSAGES,?,?),btype(INTEGER,?,?));Type(requestPaperDelivery) == Cst(etype(MESSAGES,?,?),btype(INTEGER,?,?)*etype(NEWSPAPERS,?,?)));
  Observers(Machine(Newspaper)) == (Type(papersSold) == Cst(btype(INTEGER,?,?),etype(NEWSPAPERS,?,?));Type(whatPapersSold) == Cst(SetOf(etype(NEWSPAPERS,?,?)),No_type);Type(whatPaperDelivered) == Cst(etype(NEWSPAPERS,?,?),btype(INTEGER,?,?));Type(housesDeliveredTo) == Cst(SetOf(btype(INTEGER,?,?)),No_type))
END
&
THEORY TCIntRdX IS
  predB0 == OK;
  extended_sees == KO;
  B0check_tab == KO;
  local_op == OK;
  abstract_constants_visible_in_values == KO;
  project_type == SOFTWARE_TYPE;
  event_b_deadlockfreeness == KO;
  variant_clause_mandatory == KO;
  event_b_coverage == KO;
  event_b_exclusivity == KO;
  genFeasibilityPO == KO
END
)
